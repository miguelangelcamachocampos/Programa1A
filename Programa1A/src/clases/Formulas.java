/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package clases;

import java.util.ArrayList;
import javax.swing.JOptionPane;
import javax.swing.JTextArea;
import javax.swing.JTextField;

/**
 *
 * @author Miguel
 */
public class Formulas {

    static double media;
    static double suma;
    static double suma2;
    static double desviacion;
    static ArrayList<Double> li = new ArrayList<Double>();
    static ArrayList<Double> li2 = new ArrayList<Double>();

    /**
     *
     * @param n
     */
    public static void agregar(double n) {
        li.add(n);    }

    /**
     *
     * @param b
     */
    public static void Media(JTextField b) {
        if (li.isEmpty()) {
            JOptionPane.showMessageDialog(null, "Debe ingresar un elemento");
        } else {
            li.forEach((c) -> {
                suma += +c;
            });
            media = suma / li.size();
            b.setText(String.valueOf(String.format("%.2f", media))); }
    }

    /**
     *
     * @param z
     */
    public static void Desviacion(JTextField z) {
        for (int i = 0; i < li.size(); i++) {
            li2.add(Math.pow(li.get(i) - media, 2));
        }
        li2.forEach((cal) -> {
            suma2 += +cal;
        });
        desviacion = Math.sqrt(suma2 / (li2.size() - 1));
        z.setText(String.valueOf(String.format("%.2f", desviacion)));
    }

    /**
     *
     * @param s
     */
    public static void imprime(JTextArea s) {
        s.setText(String.valueOf(li)); }
    
    public static void limpiar(JTextArea a){
        li2.removeAll(li);
        li.removeAll(li2);
        li2.clear();
        li.clear();
    }
}
